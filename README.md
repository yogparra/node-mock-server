
### proyecto
```
server-mock
```

## iniciando el proyecto
```
git init
npm init -y
```

## trello
```
    https://trello.com/b/baCphY5C/monki
```

## paquetes
```
    npm install -g json-server
```

## ejecutar
```
    json-server --watch db.json --port 3001
```

## empoint
```
personas
    get  http://localhost:3001/personas
    post http://localhost:3001/personas
    { 
		"id": 2, 
		"nombre": "Guillermo", 
		"avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/101.jpg", 
		"rol": "admin" 
	}
empresas    
    get http://localhost:3001/empresas
documentos    
    get http://localhost:3001/documentos
    get http://localhost:3001/documentos/2
carpetas    
    get http://localhost:3001/carpetas
```


## ejemplos
```
    jsonplaceholder.org
    mockapi.io
```

## formato
```
    escritura_pública_de_constitución_de_la_sociedad
    certificado_de_rut_de_la_ empresa
    organigrama_corporativo
    plan_de_prevención_de_riesgos_laborales
    programas_de_prevención_de_riesgos_laborales_personalizados
    certificado_de_adhesión_a_mutualidad
    certificado_de_tasa_de_siniestralidad
    plan_de_gestión_ambiental
    procedimientos_de_trabajo_revisados_y_evaluados
    currículum_vitae_del_asesor_en_prevención_con_certificación_sns
    difusión_de_protocolos_de_salud_del_ministerio_de_salud
    reglamento_interno_de_la_empresa
```

## link
```
    https://drive.google.com/file/d/1ivPyQQJMDAAfjF3gZzVZf-84nxmH-ldh
```

## tablas-mock
```
    empresa
    {
        "nombre": "Constructora Jes SPA",
        "id": "1"
    }
    documento
    {
        "nombre": "Jose Perez",
        "archivo": "Contrato.pdf",
        "tipo": "trabajador",
        "id": "1",
        "empresaId": "1"
    },
    persona
    {
        "nombre": "Jaco",
        "avatar": "https://cloudflare-ipfs.com/ipfs/Qmd3W5DuhgHirLHGVixi6V76LhCkZUz6pnFt5AJBiyvHye/avatar/98.jpg",
        "rol": "admin",
        "id": "1"
    }
    carpeta
    {
        "id": "1",
        "nombre": "Hormigon II",
        "fechaIni": "2022-06-20T08:03:30.325Z",
        "fechaFin": "2022-09-20T16:08:57.781Z",
        "dataEmpresa": {},
        "dataPersonas": {},
        "dataHistorico": {}
    },
```

## por definir
¿Que guarda la carpeta?
    documentos (persona, empresa, historicos)
    personas/trabajadores
    configuraciones propias de la carpeta (fechas inicio/termino)
    

```
    "dataEmpresa": {}, 
    "dataPersonas": {}, 
    "dataHistorico": {}
```

